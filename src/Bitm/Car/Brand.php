<?php
namespace PHP40\Bitm\Car;
/**
 * Created by PhpStorm.
 * User: rusdid
 * Date: 2/11/17
 * Time: 6:36 PM
 */

use PHP40\Utility\Message;
use PHP40\Utility\Debug;
use PHP40\Utility\Url;
class Brand{

    public $conn = '';

    public function __construct($db_con){
        $this->conn = $db_con;
    }

    function getAllBrands(){
        global $DB_con;

        $query = 'SELECT * FROM brands';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $brands=[];
        while($brand = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $brands[] = $brand;
        }

        return $brands;

    }
    function store($data){


        try{
            $stmt = $this->conn->prepare("INSERT INTO brands(title) VALUES(:title)");
            $stmt->bindparam(":title",$data['brand']);
            $result = $stmt->execute();

            if($result){
                Message::write(Message::ADD_SUCCESS);
                Url::redirect();
                return;
            }

            Message::write(Message::MESSAGE_FAIL);
            Url::redirect('/create.php');


        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }


    function delete($id=null){

        if(!isset($id)){
            Url::redirect();
            return;
        }

        /* TODO
         *
         * If the user has permission to delete this item
         */

        try{
            $stmt = $this->conn->prepare("DELETE FROM brands WHERE id = :id");
            $stmt->bindparam(":id",$id);
            $result = $stmt->execute();

            if($result){
                Message::write(Message::DELETE_SUCCESS);
                Url::redirect();
                return;
            }

            Message::write(Message::MESSAGE_FAIL);
            Url::redirect('/create.php');


        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function get($id = null){
        if(!isset($id)){
            Url::redirect();
            return;
        }

        try{
            $stmt = $this->conn->prepare("SELECT * FROM brands WHERE id = :id");
            $stmt->bindparam(":id",$id);
            $result = $stmt->execute();

            if($result){
                $brand = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $brand;
            }

            Message::write(Message::MESSAGE_FAIL);
            Url::redirect('/index.php');


        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }

}