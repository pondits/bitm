<?php
include_once('bootstrap.php');
$brand = new Brand();
$brands = $brand->getAllBrands();
?>
<?php include_once('header.php') ?>

<div class="message">
    <?php
    if(array_key_exists('message',$_GET)){
        echo $_GET['message'];
    }

    ?>
</div>

    <div>
        <nav>
            <li> <a href="create.php">Add New Brand</a></li>
            <li>Download Brand As <a href="#">XL</a></li>
            <li>Download Brand As <a href="#">PDF</a></li>
        </nav>
    </div>
<table border="1">
    <tr>
        <th>Sl</th>
        <th>Make</th>
        <th>Actions</th>
    </tr>
    <?php
    foreach($brands as $brand){
    ?>
    <tr>
        <td><?php echo $brand['id'];?></td>
        <td><?php echo $brand['title'];?></td>
        <td> <a href="edit.php?id=<?php echo $brand['id'];?>">Edit</a> | <a href="show.php?id=<?php echo $brand['id'];?>">View</a> | <a href="delete.php?id=<?php echo $brand['id'];?>">Delete</a> | Trash | Restore </td>
    </tr>
<?php
    }
   ?>
</table>

<?php //include_once('nav.php') ?>

<?php
include_once('footer.php');
?>