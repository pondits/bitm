<?php
/**
 * Created by PhpStorm.
 * User: rusdid
 * Date: 2/13/17
 * Time: 6:54 PM
 */

namespace PHP40\Utility;


class Message {

    const ADD_SUCCESS = 'Data is added successfully';
    const UPDATE_SUCCESS = 'Data is updated successfully';
    const DELETE_SUCCESS = 'Data is deleted successfully';
    const MESSAGE_FAIL = 'An internal error is occured. Please try again later';

    public static function write($message = ''){

        $_SESSION['message'] = $message;
    }

    public static function read(){
        if(array_key_exists('message',$_SESSION)){
            return $_SESSION['message'];
        }
        return null;
    }

    public static function flush(){
        $message = null;
        if(array_key_exists('message', $_SESSION)){
            $message = $_SESSION['message'];
            $_SESSION['message'] = '';
        }
        return $message;
    }

}