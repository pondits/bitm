<?php

namespace PHP40\Utility;
/**
 * Created by PhpStorm.
 * User: rusdid
 * Date: 2/11/17
 * Time: 6:58 PM
 */

class Setting {

    public static $app_mode = 'dev';

    public static function init(){
        session_start();
        if(self::$app_mode == 'dev'){
            ini_set('display_errors','On');
        }else{
            ini_set('display_errors','Off');
        }
    }
}