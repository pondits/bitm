<?php
include_once ('vendor/autoload.php');

use PHP40\Utility\Setting;
use PHP40\Utility\Sanitize;
use PHP40\DB\DB;
use PHP40\Bitm\Car\Brand;
use PHP40\Utility\Url;

Setting::init();
DB::connect();

$data = Sanitize::sanitize($_GET);
$brand = new Brand(DB::$conn);
if(array_key_exists('id', $data)){
    $brand->delete($data['id']);
}else{
    Url::redirect();
}

