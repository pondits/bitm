<?php
include_once ('vendor/autoload.php');

use PHP40\Utility\Setting;
use PHP40\Utility\Message;

Setting::init();
?>

<?php include_once('header.php') ?>

    <div class="message">
        <?php
        echo Message::read();
        ?>
    </div>

<fieldset>
    <legend>Add Brand</legend>
    <form action="store.php" method="post">
    <label>Enter Brand Name</label>
    <input name="brand"
           autofocus="autofocus"
           placeholder="enter brand of a car. e.g. BMW"
            />

    <div>
    <button type="submit">Save</button>
    <button type="reset">Reset</button>
    <button type="button">Click Me!</button>
    </div>
</form>
</fieldset>
<?php include_once('nav.php') ?>
<?php include_once('footer.php') ?>