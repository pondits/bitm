<?php

class Brand{



function getAllBrands(){
    global $DB_con;

    $query = 'SELECT * FROM brands';
    $stmt = $DB_con->prepare($query);
    $stmt->execute();

    $brands=[];
    while($brand = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $brands[] = $brand;
    }

    return $brands;

}
function store($data){
    global $DB_con;

    //$query = 'INSERT INTO brands (title) VALUES("'.$data['brand'].'")';
try{
    $stmt = $DB_con->prepare("INSERT INTO brands(title) VALUES(:title)");
    $stmt->bindparam(":title",$data['brand']);
    $stmt->execute();
    return true;
}catch(PDOException $e){
    echo $e->getMessage();
}

}

}