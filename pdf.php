<?php
include_once ('vendor/autoload.php');

use PHP40\Utility\Setting;
use PHP40\DB\DB;
use PHP40\Bitm\Car\Brand;

Setting::init();
DB::connect();

$brand = new Brand(DB::$conn);
$brands = $brand->getAllBrands();

$content1 = <<<BITMP1


    <table border="1">
        <tr>
            <th>Sl</th>
            <th>Make</th>

        </tr>
BITMP1;
$content2="";
foreach($brands as $brand){


    $content2 .= "<tr>
                <td>  ".$brand['id']." </td>
                <td> ".$brand['title']." </td>
                </tr>";

}
$content3 = "</table>";

$mpdf = new mPDF();
$mpdf->WriteHTML($content1.$content2.$content3);
$mpdf->Output();
?>