<?php
include_once ('vendor/autoload.php');

use PHP40\Utility\Setting;
use PHP40\Utility\Sanitize;
use PHP40\DB\DB;
use PHP40\Bitm\Car\Brand;

Setting::init();
DB::connect();

$data = Sanitize::sanitize($_POST);
$brand = new Brand(DB::$conn);
$brand->store($data);
