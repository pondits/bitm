<?php
include_once ('vendor/autoload.php');


use PHP40\Utility\Setting;
use PHP40\DB\DB;
use PHP40\Bitm\Car\Brand;
use PHP40\Utility\Message;

Setting::init();
DB::connect();

$brand = new Brand(DB::$conn);
$brands = $brand->getAllBrands();


?>
<?php include_once('header.php') ?>

    <div class="message">
        <?php
        echo Message::read();
        ?>
    </div>

    <div>
        <nav>
            <li> <a href="create.php">Add New Brand</a></li>
            <li>Download Brand As <a href="#">XL</a></li>
            <li>Download Brand As <a target="_blank" href="pdf.php">PDF</a></li>
        </nav>
    </div>
    <table border="1">
        <tr>
            <th>Sl</th>
            <th>Make</th>
            <th>Actions</th>
        </tr>
        <?php
        foreach($brands as $brand){
            ?>
            <tr>
                <td><?php echo $brand['id'];?></td>
                <td><?php echo $brand['title'];?></td>
                <td> <a href="edit.php?id=<?php echo $brand['id'];?>">Edit</a> | <a href="show.php?id=<?php echo $brand['id'];?>">View</a> | <a href="delete.php?id=<?php echo $brand['id'];?>">Delete</a> | Trash | Restore </td>
            </tr>
        <?php
        }
        ?>
    </table>

<?php //include_once('nav.php') ?>

<?php
include_once('footer.php');
?>