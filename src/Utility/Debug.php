<?php
/**
 * Created by PhpStorm.
 * User: rusdid
 * Date: 2/13/17
 * Time: 7:08 PM
 */

namespace PHP40\Utility;


class Debug {

    public static function debug($var){

        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    public static function d($var){
        self::debug($var);
    }

    public static function dd($var){
        self::debug($var);
        die();
    }
} 