<?php

namespace PHP40\DB;
/**
 * Created by PhpStorm.
 * User: rusdid
 * Date: 2/11/17
 * Time: 6:47 PM
 */

class DB {

    const DB_HOST = 'localhost';
    const DB_NAME = 'bitm';
    const DB_USER = 'bitmuser';
    const DB_PASSWORD = 'bitmpassword';

    public static $conn = '';


    public static function connect(){
        try
        {
            self::$conn = new \PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_NAME,self::DB_USER,self::DB_PASSWORD);
            self::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
} 