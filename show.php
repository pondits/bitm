<?php
include_once ('vendor/autoload.php');


use PHP40\Utility\Setting;
use PHP40\DB\DB;
use PHP40\Bitm\Car\Brand;
use PHP40\Utility\Url;
use PHP40\Utility\Sanitize;

Setting::init();
DB::connect();

$data = Sanitize::sanitize($_GET);
$brand = new Brand(DB::$conn);
if(array_key_exists('id', $data)){
    $brand = $brand->get($data['id']);
}else{
    $brand = null;
}

if(is_null($brand)){
  Url::redirect();
}

?>
<?php include_once('header.php') ?>

    <dl>
        <dt>Id:<?php echo $brand['id']?></dt>
        <dd>Title: <?php echo $brand['title']?></dd>
    </dl>

<?php include_once('nav.php') ?>
<?php include_once('footer.php') ?>
